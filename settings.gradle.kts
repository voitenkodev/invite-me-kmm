enableFeaturePreview("VERSION_CATALOGS")

rootProject.name = "Invite Me"

include(
    ":core:remote",
    ":core:local",
    ":core:mvi",
)

include(
    ":apps:android",
    ":apps:ios",
    ":apps:desktop"
)

includeBuild("build-conventions")

pluginManagement {
    resolutionStrategy {
        eachPlugin {
            when (requested.id.id) {
                "com.squareup.sqldelight" -> useModule("com.squareup.sqldelight:gradle-plugin:1.5.2")
            }
            when (requested.id.namespace) {
                "com.android" -> useModule("com.android.tools.build:gradle:7.1.0-beta02")
                "org.jetbrains.kotlin" -> useModule("org.jetbrains.kotlin:kotlin-gradle-plugin:1.5.31")
            }
        }
    }

    repositories {
        google()
        gradlePluginPortal()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }
}

@Suppress("UnstableApiUsage")
dependencyResolutionManagement {
    repositories {
        mavenCentral()
        google()
    }

    versionCatalogs { create("libs") { from(files("gradle/wrapper/libs.versions.toml")) } }
}
