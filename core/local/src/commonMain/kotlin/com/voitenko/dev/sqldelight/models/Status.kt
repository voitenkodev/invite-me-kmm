package com.voitenko.dev.sqldelight.models

public enum class Status { ACTIVE, USED, UNKNOWN }
