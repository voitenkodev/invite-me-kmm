package com.voitenko.dev.sqldelight.core

import com.squareup.sqldelight.db.SqlDriver

public expect class DatabaseDriverFactory {
    public fun createDriver(): SqlDriver
}