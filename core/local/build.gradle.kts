@Suppress("UnstableApiUsage", "DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.kmm.convention)
    alias(libs.plugins.kmm.implementation)
    alias(libs.plugins.sqldelight)
}

`kmm-implementation`.apply {
    implementation(common = libs.kotlinx.coroutines)
    implementation(common = libs.kotlinx.datetime)
    implementations(
        common = listOf(libs.sqldelight.common, libs.sqldelight.extensions),
        android = listOf(libs.sqldelight.android),
        ios = listOf(libs.sqldelight.native),
        macos = listOf(libs.sqldelight.native),
        windows = listOf(libs.sqldelight.native),
        linux = listOf(libs.sqldelight.native),
    )
}

sqldelight {
    this.database("AppDataBase") {
        packageName = "com.voitenko.dev.sqldelight.core"
        sourceFolders = listOf("kotlin")
    }
}
