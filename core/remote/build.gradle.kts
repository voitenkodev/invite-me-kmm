@Suppress("UnstableApiUsage", "DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.kmm.convention)
    alias(libs.plugins.kmm.implementation)
}

`kmm-implementation`.apply {
    implementation(common = libs.kotlinx.coroutines)
    implementation(common = libs.kotlinx.serialization.json)
    implementations(
        common = listOf(libs.ktor.core, libs.ktor.logging, libs.ktor.serialization),
        ios = listOf(libs.ktor.ios),
        android = listOf(libs.ktor.okhttp),
        macos = listOf(libs.ktor.native),
        windows = listOf(libs.ktor.native),
        linux = listOf(libs.ktor.native)
    )
}