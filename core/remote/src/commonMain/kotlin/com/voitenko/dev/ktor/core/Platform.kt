package com.voitenko.dev.ktor.core

import io.ktor.client.*

internal expect object KtorFactory {
    internal fun client(): HttpClient
}