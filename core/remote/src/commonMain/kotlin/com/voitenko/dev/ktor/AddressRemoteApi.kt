package com.voitenko.dev.ktor

import com.voitenko.dev.ktor.core.Client
import com.voitenko.dev.ktor.models.AddressResponse
import io.ktor.client.*
import io.ktor.client.request.*

public class AddressRemoteApi(private val service: HttpClient = Client.address()) {

    public suspend fun getAddressByLatLng(lat: Double, lng: Double): AddressResponse? =
        service.get {
            url {
                path("geocoding/v5/mapbox.places/${lng},${lat}.json")
                parameter("limit", "1")
            }
        }
}