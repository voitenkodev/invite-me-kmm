package com.voitenko.dev.ktor.core

import io.ktor.client.*
import io.ktor.client.engine.curl.*

internal actual object KtorFactory {
    internal actual fun client() = HttpClient(Curl)
}