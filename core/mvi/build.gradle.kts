@Suppress("UnstableApiUsage", "DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.kmm.convention)
    alias(libs.plugins.kmm.implementation)
}

`kmm-implementation`.apply {
    implementation(common = libs.kotlinx.coroutines)
}