package com.voitenko.dev.invitemekmm.android.features.bottomSheet

import com.voitenko.dev.mvi.feature.Actor
import com.voitenko.dev.mvi.feature.Feature
import com.voitenko.dev.mvi.feature.Reducer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.flowOf

class BottomSheetFeature constructor(
    scope: CoroutineScope,
    initial: State = State()
) : Feature<BottomSheetFeature.Wish, BottomSheetFeature.State, Nothing>(
    scope = scope,
    initial = initial,
    actor = ActorImpl(),
    reducer = ReducerImpl(),
) {

    sealed class Wish : Feature.Wish {
        object OpenSheet : Wish()
        object CloseSheet : Wish()
    }

    data class State(val isOpen: Boolean = false) : Feature.State

    class ActorImpl : Actor<Wish, State> {
        override fun invoke(wish: Wish, state: State) = when (wish) {
            else -> flowOf(wish)
        }
    }

    class ReducerImpl : Reducer<Wish, State> {
        override fun invoke(wish: Wish, state: State) = when (wish) {
            is Wish.OpenSheet -> state.copy(isOpen = true)
            is Wish.CloseSheet -> state.copy(isOpen = false)
        }
    }
}
